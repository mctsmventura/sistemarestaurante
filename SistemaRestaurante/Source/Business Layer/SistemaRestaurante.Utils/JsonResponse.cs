﻿
namespace SistemaRestaurante.Utils
{
    public class JsonResponse
    {
        public string Message { set; get; }
        public bool Success { set; get; }
        public object Data { set; get; }
    }
}
