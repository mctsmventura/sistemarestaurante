﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.DataAcess
{
    using System.Data;

    using Microsoft.Practices.EnterpriseLibrary.Data;

    using SistemaRestaurante.BusinessEntity;

    public class ClienteDAL : Singleton<ClienteDAL>
    {
        private readonly Database BaseDatos = DatabaseFactory.CreateDatabase();

        public IList<Cliente> Listar()
        {
            var clientes = new List<Cliente>();
            var comando = BaseDatos.GetStoredProcCommand("usp_SelectAll_Cliente");

            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                while (lector.Read())
                {
                    clientes.Add(new Cliente
                    {
                        IdCliente = lector.GetInt32(lector.GetOrdinal("IdCliente")),
                        Nombre = lector.GetString(lector.GetOrdinal("Nombre")),
                        Documento= lector.GetString(lector.GetOrdinal("Documento"))
                    });
                }
            }
            comando.Dispose();
            return clientes;
        }

        public Cliente Obtener(int idCliente)
        {
            var cliente = new Cliente();
            var comando = BaseDatos.GetStoredProcCommand("usp_Select_Cliente");
            BaseDatos.AddInParameter(comando, "IdCliente", DbType.Int32, idCliente);
            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                if (lector.Read())
                {
                    cliente.IdCliente = lector.GetInt32(lector.GetOrdinal("IdCliente"));
                    cliente.Nombre = lector.GetString(lector.GetOrdinal("Nombre"));
                    cliente.Documento = lector.GetString(lector.GetOrdinal("Documento"));
                }
            }
            comando.Dispose();
            return cliente;
        }

        public int Agregar(Cliente cliente)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Insert_Cliente");
            BaseDatos.AddOutParameter(comando, "IdCliente", DbType.Int32, 4);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, cliente.Nombre);
            BaseDatos.AddInParameter(comando, "Documento", DbType.String, cliente.Documento);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo insertar el registro en la base de datos");

            var valor = (int)BaseDatos.GetParameterValue(comando, "IdCliente");
            comando.Dispose();

            return valor;
        }

        public bool Modificar(Cliente cliente)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Update_Cliente");
            BaseDatos.AddInParameter(comando, "IdCliente", DbType.Int32, cliente.IdCliente);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, cliente.Nombre);
            BaseDatos.AddInParameter(comando, "Documento", DbType.String, cliente.Documento);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("Hubo un error al modificar");

            comando.Dispose();

            return true;

        }

        public bool Eliminar(int idCliente)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Delete_Cliente");
            BaseDatos.AddInParameter(comando, "IdCliente", DbType.Int32, idCliente);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo eliminar el registro");

            comando.Dispose();

            return true;
        }
    }
}
