﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.DataAcess
{
    using System.Data;

    using Microsoft.Practices.EnterpriseLibrary.Data;

    using SistemaRestaurante.BusinessEntity;

    public class ProductoDAL : Singleton<ProductoDAL>
    {
        private readonly Database BaseDatos = DatabaseFactory.CreateDatabase();

        public IList<Producto> Listar()
        {
            var productos = new List<Producto>();
            var comando = BaseDatos.GetStoredProcCommand("usp_SelectAll_Producto");

            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                while (lector.Read())
                {
                    productos.Add(new Producto
                    {
                        IdProducto = lector.GetInt32(lector.GetOrdinal("IdProducto")),
                        Nombre = lector.GetString(lector.GetOrdinal("Nombre")),
                        Descripcion = lector.GetString(lector.GetOrdinal("Descripcion")),
                        Imagen = lector.GetString(lector.GetOrdinal("Imagen")),
                        IdCategoria = lector.GetInt32(lector.GetOrdinal("IdCategoria")),
                        Precio = lector.GetDecimal(lector.GetOrdinal("Precio")),
                        NombreCategoria = lector.GetString(lector.GetOrdinal("NombreCategoria"))
                    });
                }
            }
            comando.Dispose();
            return productos;
        }

        public Producto Obtener(int idProducto)
        {
            var local = new Producto();
            var comando = BaseDatos.GetStoredProcCommand("usp_Select_Producto");
            BaseDatos.AddInParameter(comando, "IdProducto", DbType.Int32, idProducto);
            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                if (lector.Read())
                {
                    local.IdProducto = lector.GetInt32(lector.GetOrdinal("IdProducto"));
                    local.Nombre = lector.GetString(lector.GetOrdinal("Nombre"));
                    local.Descripcion = lector.GetString(lector.GetOrdinal("Descripcion"));
                    local.Imagen = lector.GetString(lector.GetOrdinal("Imagen"));
                    local.IdCategoria = lector.GetInt32(lector.GetOrdinal("IdCategoria"));
                    local.Precio = lector.GetDecimal(lector.GetOrdinal("Precio"));
                    local.NombreCategoria = lector.GetString(lector.GetOrdinal("NombreCategoria"));
                }
            }
            comando.Dispose();
            return local;
        }

        public int Agregar(Producto local)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Insert_Producto");
            BaseDatos.AddOutParameter(comando, "IdProducto", DbType.Int32, 4);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, local.Nombre);
            BaseDatos.AddInParameter(comando, "Descripcion", DbType.String, local.Descripcion);
            BaseDatos.AddInParameter(comando, "Imagen", DbType.String, local.Imagen);
            BaseDatos.AddInParameter(comando, "IdCategoria", DbType.Int32, local.IdCategoria);
            BaseDatos.AddInParameter(comando, "Precio", DbType.Decimal, local.Precio);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo insertar el registro en la base de datos");

            var valor = (int)BaseDatos.GetParameterValue(comando, "IdProducto");
            comando.Dispose();

            return valor;
        }

        public bool Modificar(Producto local)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Update_Producto");
            BaseDatos.AddInParameter(comando, "IdProducto", DbType.Int32, local.IdProducto);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, local.Nombre);
            BaseDatos.AddInParameter(comando, "Descripcion", DbType.String, local.Descripcion);
            BaseDatos.AddInParameter(comando, "Imagen", DbType.String, local.Imagen);
            BaseDatos.AddInParameter(comando, "IdCategoria", DbType.Int32, local.IdCategoria);
            BaseDatos.AddInParameter(comando, "Precio", DbType.Decimal, local.Precio);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("Hubo un error al modificar");

            comando.Dispose();

            return true;

        }

        public bool Eliminar(int idProducto)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Delete_Producto");
            BaseDatos.AddInParameter(comando, "IdProducto", DbType.Int32, idProducto);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo eliminar el registro");

            comando.Dispose();

            return true;
        }
    }
}
