﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;
using SistemaRestaurante.BusinessEntity;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace SistemaRestaurante.DataAcess
{
    public class UsuarioDAL : Singleton<UsuarioDAL>
    {
        private readonly Database BaseDatos = DatabaseFactory.CreateDatabase();

        public Usuario Obtener(string username, string password)
        {
            var usuario = new Usuario();
            var comando = BaseDatos.GetStoredProcCommand("usp_ObtenerUsuarioLogin");
            BaseDatos.AddInParameter(comando, "Username", DbType.String, username);
            BaseDatos.AddInParameter(comando, "Password", DbType.String, password);

            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                if (lector.Read())
                {
                    usuario.IdUsuario= lector.GetInt32(lector.GetOrdinal("IdUsuario"));
                    usuario.Username = lector.GetString(lector.GetOrdinal("Username"));
                    usuario.Password = lector.GetString(lector.GetOrdinal("Password"));
                    usuario.Rol = lector.GetInt32(lector.GetOrdinal("Rol"));
                    usuario.Email = lector.IsDBNull(lector.GetOrdinal("Email")) == true ? string.Empty : lector.GetString(lector.GetOrdinal("Email"));
                    usuario.IdCliente = lector.IsDBNull(lector.GetOrdinal("IdCliente")) ==true ? 0: lector.GetInt32(lector.GetOrdinal("IdCliente"));
                }
            }
            comando.Dispose();
            return usuario;
        }
    }
}
